# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.7](https://gitlab.com/viniciuspjardim/turborepo-test/compare/v0.0.7-beta.0...v0.0.7) (2022-04-08)

### [0.0.7-beta.0](https://gitlab.com/viniciuspjardim/turborepo-test/compare/v0.0.6...v0.0.7-beta.0) (2022-04-08)


### Features

* integration change  log ([f131376](https://gitlab.com/viniciuspjardim/turborepo-test/commit/f1313765d0dc4cbfe8ede5572e4910506340253d))

### [0.0.6](https://gitlab.com/viniciuspjardim/turborepo-test/compare/v0.0.5...v0.0.6) (2022-04-04)


### Features

* change title to 3 ([47333b6](https://gitlab.com/viniciuspjardim/turborepo-test/commit/47333b669fedcb13825c16835eb7f864b5fe6c89))
* make git commands simpler ([d76a3e6](https://gitlab.com/viniciuspjardim/turborepo-test/commit/d76a3e6a96e192d37aea80db0c4268772e4efcdf))


### Bug Fixes

* change ci changelog branch ([748d5a4](https://gitlab.com/viniciuspjardim/turborepo-test/commit/748d5a4db11b59e5373aeb51cba3274f6165373b))

### [0.0.5](https://gitlab.com/viniciuspjardim/turborepo-test/compare/v0.0.4...v0.0.5) (2022-04-04)


### Features

* new title 2 ([34ddc6d](https://gitlab.com/viniciuspjardim/turborepo-test/commit/34ddc6d107b6581e258d1bf9cc9aefcde67fbfcd))

### 0.0.4 (2022-04-04)


### Features

* change git email and username ([65b0066](https://gitlab.com/viniciuspjardim/turborepo-test/commit/65b0066dc0965525aa85ea225484b42104af2880))
* change title ([cdf5f4f](https://gitlab.com/viniciuspjardim/turborepo-test/commit/cdf5f4ff6949ce20de79d16386588a4edee68c96))
* configure git on ci ([8779c8f](https://gitlab.com/viniciuspjardim/turborepo-test/commit/8779c8fd178b5d7244937f392e3516a802ef9d5d))
* configure SSH private key ([d59e992](https://gitlab.com/viniciuspjardim/turborepo-test/commit/d59e992ebe0a3c41f6f2ecfda2336b6824f0bfa6))
* generate changelog and bump version on ci ([eba18a7](https://gitlab.com/viniciuspjardim/turborepo-test/commit/eba18a75199342bb5f3259fdadc79ee331a72e3b))
* log ssh key ([b9d4789](https://gitlab.com/viniciuspjardim/turborepo-test/commit/b9d4789c402e17f5a88bdbe6bd2c1796379901c9))
* move changelog ci to production ([b72f677](https://gitlab.com/viniciuspjardim/turborepo-test/commit/b72f67789f093e6ec26a84fa4019942c8ac32222))
* push changelog and additional debug ([884bb5a](https://gitlab.com/viniciuspjardim/turborepo-test/commit/884bb5a3a4928124f765e60bd75d44f9f234681c))
* remove some logs ([ff24a5a](https://gitlab.com/viniciuspjardim/turborepo-test/commit/ff24a5a285e0877ce97d7985c33ede65b6bb8a4f))
* test 1 ([b5b4117](https://gitlab.com/viniciuspjardim/turborepo-test/commit/b5b4117016c6f8884a613cbc42463e8806fb1b15))
* test 2 ([dc1a9a0](https://gitlab.com/viniciuspjardim/turborepo-test/commit/dc1a9a09b9bc5e2d1400fb1363048cdc323a9aa3))
* test 3 ([678fa90](https://gitlab.com/viniciuspjardim/turborepo-test/commit/678fa90705903be3d75c70aaaacf85dca64e2f85))
* test 4 ([59bddc1](https://gitlab.com/viniciuspjardim/turborepo-test/commit/59bddc1910207763c8ee6c6b68646cdc5eed597b))
* try to push commit from gitlab ci ([5f775be](https://gitlab.com/viniciuspjardim/turborepo-test/commit/5f775bef50dfc0d82fb9dc6abe7e178bddb4333a))


### Bug Fixes

* change commit to main ([9596837](https://gitlab.com/viniciuspjardim/turborepo-test/commit/9596837f55eba1e9726d7a0ef0480f6e90ba83b3))
* ci fo --follow-tags and -o ci.skip ([66d7822](https://gitlab.com/viniciuspjardim/turborepo-test/commit/66d78221b77bcbd0d01af2aeb8313ae42e7777bf))
* fix gitlab ssh ci config ([15f1400](https://gitlab.com/viniciuspjardim/turborepo-test/commit/15f1400550036d54b356a6a64abfbe4f9f514ed4))
* remove public key ([5409303](https://gitlab.com/viniciuspjardim/turborepo-test/commit/54093038a06069c9c06042c3427768c38dbecc2d))
* remove space ([1cacdfa](https://gitlab.com/viniciuspjardim/turborepo-test/commit/1cacdfa4b41dad4ff8f9d704d23e3cdcc78662c8))
* test 5 ([db9524a](https://gitlab.com/viniciuspjardim/turborepo-test/commit/db9524a5ede6eb7b78e62ea64156e12f589a722f))
* test 6 ([1f878b8](https://gitlab.com/viniciuspjardim/turborepo-test/commit/1f878b82c8e669347cfc2a4e09fca10fc3f97fd9))
* test 7 ([8dc8799](https://gitlab.com/viniciuspjardim/turborepo-test/commit/8dc8799891bc8d6abc65db6e8e4c15ffd66a8faa))

### 0.0.3 (2022-04-04)


### Features

* change git email and username ([65b0066](https://gitlab.com/viniciuspjardim/turborepo-test/commit/65b0066dc0965525aa85ea225484b42104af2880))
* configure git on ci ([8779c8f](https://gitlab.com/viniciuspjardim/turborepo-test/commit/8779c8fd178b5d7244937f392e3516a802ef9d5d))
* configure SSH private key ([d59e992](https://gitlab.com/viniciuspjardim/turborepo-test/commit/d59e992ebe0a3c41f6f2ecfda2336b6824f0bfa6))
* generate changelog and bump version on ci ([eba18a7](https://gitlab.com/viniciuspjardim/turborepo-test/commit/eba18a75199342bb5f3259fdadc79ee331a72e3b))
* log ssh key ([b9d4789](https://gitlab.com/viniciuspjardim/turborepo-test/commit/b9d4789c402e17f5a88bdbe6bd2c1796379901c9))
* move changelog ci to production ([b72f677](https://gitlab.com/viniciuspjardim/turborepo-test/commit/b72f67789f093e6ec26a84fa4019942c8ac32222))
* push changelog and additional debug ([884bb5a](https://gitlab.com/viniciuspjardim/turborepo-test/commit/884bb5a3a4928124f765e60bd75d44f9f234681c))
* remove some logs ([ff24a5a](https://gitlab.com/viniciuspjardim/turborepo-test/commit/ff24a5a285e0877ce97d7985c33ede65b6bb8a4f))
* test 1 ([b5b4117](https://gitlab.com/viniciuspjardim/turborepo-test/commit/b5b4117016c6f8884a613cbc42463e8806fb1b15))
* test 2 ([dc1a9a0](https://gitlab.com/viniciuspjardim/turborepo-test/commit/dc1a9a09b9bc5e2d1400fb1363048cdc323a9aa3))
* test 3 ([678fa90](https://gitlab.com/viniciuspjardim/turborepo-test/commit/678fa90705903be3d75c70aaaacf85dca64e2f85))
* test 4 ([59bddc1](https://gitlab.com/viniciuspjardim/turborepo-test/commit/59bddc1910207763c8ee6c6b68646cdc5eed597b))
* try to push commit from gitlab ci ([5f775be](https://gitlab.com/viniciuspjardim/turborepo-test/commit/5f775bef50dfc0d82fb9dc6abe7e178bddb4333a))


### Bug Fixes

* change commit to main ([9596837](https://gitlab.com/viniciuspjardim/turborepo-test/commit/9596837f55eba1e9726d7a0ef0480f6e90ba83b3))
* fix gitlab ssh ci config ([15f1400](https://gitlab.com/viniciuspjardim/turborepo-test/commit/15f1400550036d54b356a6a64abfbe4f9f514ed4))
* remove public key ([5409303](https://gitlab.com/viniciuspjardim/turborepo-test/commit/54093038a06069c9c06042c3427768c38dbecc2d))
* remove space ([1cacdfa](https://gitlab.com/viniciuspjardim/turborepo-test/commit/1cacdfa4b41dad4ff8f9d704d23e3cdcc78662c8))
* test 5 ([db9524a](https://gitlab.com/viniciuspjardim/turborepo-test/commit/db9524a5ede6eb7b78e62ea64156e12f589a722f))
* test 6 ([1f878b8](https://gitlab.com/viniciuspjardim/turborepo-test/commit/1f878b82c8e669347cfc2a4e09fca10fc3f97fd9))
* test 7 ([8dc8799](https://gitlab.com/viniciuspjardim/turborepo-test/commit/8dc8799891bc8d6abc65db6e8e4c15ffd66a8faa))

### 0.0.2 (2022-04-04)


### Features

* change git email and username ([65b0066](https://gitlab.com/viniciuspjardim/turborepo-test/commit/65b0066dc0965525aa85ea225484b42104af2880))
* configure git on ci ([8779c8f](https://gitlab.com/viniciuspjardim/turborepo-test/commit/8779c8fd178b5d7244937f392e3516a802ef9d5d))
* configure SSH private key ([d59e992](https://gitlab.com/viniciuspjardim/turborepo-test/commit/d59e992ebe0a3c41f6f2ecfda2336b6824f0bfa6))
* generate changelog and bump version on ci ([eba18a7](https://gitlab.com/viniciuspjardim/turborepo-test/commit/eba18a75199342bb5f3259fdadc79ee331a72e3b))
* log ssh key ([b9d4789](https://gitlab.com/viniciuspjardim/turborepo-test/commit/b9d4789c402e17f5a88bdbe6bd2c1796379901c9))
* move changelog ci to production ([b72f677](https://gitlab.com/viniciuspjardim/turborepo-test/commit/b72f67789f093e6ec26a84fa4019942c8ac32222))
* push changelog and additional debug ([884bb5a](https://gitlab.com/viniciuspjardim/turborepo-test/commit/884bb5a3a4928124f765e60bd75d44f9f234681c))
* remove some logs ([ff24a5a](https://gitlab.com/viniciuspjardim/turborepo-test/commit/ff24a5a285e0877ce97d7985c33ede65b6bb8a4f))
* test 1 ([b5b4117](https://gitlab.com/viniciuspjardim/turborepo-test/commit/b5b4117016c6f8884a613cbc42463e8806fb1b15))
* test 2 ([dc1a9a0](https://gitlab.com/viniciuspjardim/turborepo-test/commit/dc1a9a09b9bc5e2d1400fb1363048cdc323a9aa3))
* test 3 ([678fa90](https://gitlab.com/viniciuspjardim/turborepo-test/commit/678fa90705903be3d75c70aaaacf85dca64e2f85))
* test 4 ([59bddc1](https://gitlab.com/viniciuspjardim/turborepo-test/commit/59bddc1910207763c8ee6c6b68646cdc5eed597b))
* try to push commit from gitlab ci ([5f775be](https://gitlab.com/viniciuspjardim/turborepo-test/commit/5f775bef50dfc0d82fb9dc6abe7e178bddb4333a))


### Bug Fixes

* change commit to main ([9596837](https://gitlab.com/viniciuspjardim/turborepo-test/commit/9596837f55eba1e9726d7a0ef0480f6e90ba83b3))
* fix gitlab ssh ci config ([15f1400](https://gitlab.com/viniciuspjardim/turborepo-test/commit/15f1400550036d54b356a6a64abfbe4f9f514ed4))
* remove public key ([5409303](https://gitlab.com/viniciuspjardim/turborepo-test/commit/54093038a06069c9c06042c3427768c38dbecc2d))
* remove space ([1cacdfa](https://gitlab.com/viniciuspjardim/turborepo-test/commit/1cacdfa4b41dad4ff8f9d704d23e3cdcc78662c8))
* test 5 ([db9524a](https://gitlab.com/viniciuspjardim/turborepo-test/commit/db9524a5ede6eb7b78e62ea64156e12f589a722f))
* test 6 ([1f878b8](https://gitlab.com/viniciuspjardim/turborepo-test/commit/1f878b82c8e669347cfc2a4e09fca10fc3f97fd9))
* test 7 ([8dc8799](https://gitlab.com/viniciuspjardim/turborepo-test/commit/8dc8799891bc8d6abc65db6e8e4c15ffd66a8faa))

### 0.0.1 (2022-04-04)


### Features

* change git email and username ([65b0066](https://gitlab.com/viniciuspjardim/turborepo-test/commit/65b0066dc0965525aa85ea225484b42104af2880))
* configure git on ci ([8779c8f](https://gitlab.com/viniciuspjardim/turborepo-test/commit/8779c8fd178b5d7244937f392e3516a802ef9d5d))
* configure SSH private key ([d59e992](https://gitlab.com/viniciuspjardim/turborepo-test/commit/d59e992ebe0a3c41f6f2ecfda2336b6824f0bfa6))
* generate changelog and bump version on ci ([eba18a7](https://gitlab.com/viniciuspjardim/turborepo-test/commit/eba18a75199342bb5f3259fdadc79ee331a72e3b))
* log ssh key ([b9d4789](https://gitlab.com/viniciuspjardim/turborepo-test/commit/b9d4789c402e17f5a88bdbe6bd2c1796379901c9))
* push changelog and additional debug ([884bb5a](https://gitlab.com/viniciuspjardim/turborepo-test/commit/884bb5a3a4928124f765e60bd75d44f9f234681c))
* remove some logs ([ff24a5a](https://gitlab.com/viniciuspjardim/turborepo-test/commit/ff24a5a285e0877ce97d7985c33ede65b6bb8a4f))
* test 1 ([b5b4117](https://gitlab.com/viniciuspjardim/turborepo-test/commit/b5b4117016c6f8884a613cbc42463e8806fb1b15))
* test 2 ([dc1a9a0](https://gitlab.com/viniciuspjardim/turborepo-test/commit/dc1a9a09b9bc5e2d1400fb1363048cdc323a9aa3))
* test 3 ([678fa90](https://gitlab.com/viniciuspjardim/turborepo-test/commit/678fa90705903be3d75c70aaaacf85dca64e2f85))
* test 4 ([59bddc1](https://gitlab.com/viniciuspjardim/turborepo-test/commit/59bddc1910207763c8ee6c6b68646cdc5eed597b))
* try to push commit from gitlab ci ([5f775be](https://gitlab.com/viniciuspjardim/turborepo-test/commit/5f775bef50dfc0d82fb9dc6abe7e178bddb4333a))


### Bug Fixes

* change commit to main ([9596837](https://gitlab.com/viniciuspjardim/turborepo-test/commit/9596837f55eba1e9726d7a0ef0480f6e90ba83b3))
* fix gitlab ssh ci config ([15f1400](https://gitlab.com/viniciuspjardim/turborepo-test/commit/15f1400550036d54b356a6a64abfbe4f9f514ed4))
* remove public key ([5409303](https://gitlab.com/viniciuspjardim/turborepo-test/commit/54093038a06069c9c06042c3427768c38dbecc2d))
* remove space ([1cacdfa](https://gitlab.com/viniciuspjardim/turborepo-test/commit/1cacdfa4b41dad4ff8f9d704d23e3cdcc78662c8))
* test 5 ([db9524a](https://gitlab.com/viniciuspjardim/turborepo-test/commit/db9524a5ede6eb7b78e62ea64156e12f589a722f))
* test 6 ([1f878b8](https://gitlab.com/viniciuspjardim/turborepo-test/commit/1f878b82c8e669347cfc2a4e09fca10fc3f97fd9))
* test 7 ([8dc8799](https://gitlab.com/viniciuspjardim/turborepo-test/commit/8dc8799891bc8d6abc65db6e8e4c15ffd66a8faa))
